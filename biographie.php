<?php get_header(); ?>

	<div class="index">
		<ul>
			<li><a href="#" class="">A</a></li>
			<li><a href="#" class="">B</a></li>
			<li><a href="#" class="">C</a></li>
			<li><a href="#" class="">D</a></li>
			<li><a href="#" class="">E</a></li>
			<li><a href="#" class="">F</a></li>
			<li><a href="#" class="">G</a></li>
			<li><a href="#" class="">H</a></li>
			<li><a href="#" class="">I</a></li>
			<li><a href="#" class="">J</a></li>
			<li><a href="#" class="">K</a></li>
			<li><a href="#" class="">L</a></li>
			<li><a href="#" class="">M</a></li>
			<li><a href="#" class="">N</a></li>
			<li><a href="#" class="">O</a></li>
			<li class="active"><a href="#" class="">P</a></li>
			<li><a href="#" class="">Q</a></li>
			<li><a href="#" class="">R</a></li>
			<li><a href="#" class="">S</a></li>
			<li><a href="#" class="">T</a></li>
			<li><a href="#" class="">U</a></li>
			<li><a href="#" class="">V</a></li>
			<li><a href="#" class="">W</a></li>
			<li><a href="#" class="">X</a></li>
			<li><a href="#" class="">Y</a></li>
			<li><a href="#" class="">Z</a></li>
		</ul>
	</div>
	<section class="biography">
		<div class="block-personality">
			<img src="./tmp/thumb-personality.jpg" class="thumb personality" alt="" width="180" height="180" />
			<div class="description-personality">
				<h2>Philippe Pierre</h2>
				<p>Ex his quidam aeternitati se commendari posse per statuas aestimantes eas ardenter adfectant quasi plus praemii de figmentis aereis sensu carentibus adepturi, quam ex conscientia honeste recteque factorum, easque auro curant inbracteari, quod Acilio Glabrioni delatum est primo, cum consiliis armisque regem superasset Antiochum. quam autem sit pulchrum exigua haec spernentem et minima ad ascensus verae gloriae tendere longos et arduos, ut memorat vates Ascraeus, Censorius Cato monstravit. qui interrogatus quam ob rem inter multos.</p>
				<aside class="personality additionals-infos">
					<h3>Fonctions et Postes occupés</h3>
					<ul class="personality ">
						<li>Quam ex conscientia honeste recteque factorum</li>
						<li>Censorius Cato monstravit</li>
					</ul>
					<h3>Distinctions</h3>
					<ul>
						<li><a href="#" target="_blank">Superasset Antiochum</a></li>
						<li><a href="#" target="_blank">Cato monstravit</a></li>
					</ul>
				</aside>
			</div>
		</div>
	</section>
	hello biographie

<?php get_footer(); ?>