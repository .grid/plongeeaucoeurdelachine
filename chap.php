<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Plongée au cœur de la Chine</title>
  <link rel="stylesheet" href="style.css">
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
  <script type="text/javascript">
	$(document).ready(function(){
		$("body").hide().fadeIn();
		$(".background-holder.home").hide(); //on cache le contenu
		//$("body").append('<div id="wait"><img src="loader.gif" alt="chargement..."/></div>');
		//$("ul.sub-menu").hide();
		//$("ul.sub-menu").hide();
		
		$("ul.top-menu li").hover( function(){
			//$(this).find('ul.sub-menu').fadeIn(200);
			$(this).children("ul.sub-menu").stop(true, false).fadeIn(600);
			//alert('yop');
		}, function() {
			//alert('yop');
			//$("ul.sub-menu").hide();
			$(this).children("ul.sub-menu").stop(true, false).fadeOut(150);
		});
		
		
		$('ul.top-menu li a').on('click', function(){
			$('ul.top-menu li').removeClass('active');
			$(this).parent('li').addClass('active');
		});
		
		$('.video-block a').on('click', function(){
			/*$.get("./video.php", { name: "John", time: "2pm" }, function(data) {
				$(".video-holder").html(data);
				alert(data);
			});*/
			$.ajax({
				type: "GET",
				url: "./video.php",
				data: { name: "John", location: "Boston" }
			}).done(function( data ) {
				$(".video-holder").empty().append(data).fadeIn();
				
				//var embeddedPlayer = $("#video-player #player iframe");
				
				$("#player").append("<div id='player-fade'></div>");
				//$("iframe").hide();
				$("#iframe").load(function(){
					$('#player-fade').fadeOut(3000);
				});
				
				$("#video-player span.btn-close").on('click', function(){
					$(".video-holder").fadeOut();
				});
				$(document).keyup(function(e) {
					//if (e.keyCode == 13) { $('.save').click(); }     // enter
					if (e.keyCode == 27) { $(".video-holder").fadeOut(); }   // esc
				});
			});
		});
		
		
		
		
		
	});
	$(window).load(function(){
		//$("#wait").hide();
		
		$(".background-holder.home").fadeIn();
	});
  </script>
</head>
<body class="homepage">
<div class="wrapper-page home">
	<header id="header">
		<div class="container">
			<nav id="main-menu">
				<ul class="top-menu">
					<li><a href="#1">Découvrir <span class="red">la Chine</span></a></li>
					<li>
						<a href="chap.php">S'installer <span class="red">en Chine</span></a>
						<ul class="sub-menu">
							<li><a href="ss-chap.php"><span class="chapter" title="Chapitre 1">1</span> Monde de l'implicite <span>Chapitre I</span></a></li>
							<li><a href="ss-chap.php"><span class="chapter" title="Chapitre 3">2</span> Rapport aux choses <span>Chapitre II</span></a></li>
							<li><a href="ss-chap.php"><span class="chapter" title="Chapitre 2">3</span> Culte de la réussite <span>Chapitre III</span></a></li>
						</ul>
					</li>
					<li><a href="#3">Travailler <span class="red">en Chine</span></a></li>
					<li><a href="#4">Regards <span class="red">Croisés</span></a></li>
				</ul>
			</nav>
		</div>
	</header>

<?php //include('./header.php'); ?>

<!--<section class="videos">
	<div class="container">
		<div class="video-block">
			<h2>Idéologie <br />de la réussite</h2>
			<a href="#">
				<span class="video-thumb-holder">
					<span class="btn-play"></span>
					<img src="./tmp/thumb-video.jpg" alt="thumb" width="178" height="100" />
					<span class="video-thumb-overlay">Voir la vidéo</span>
				</span>
			</a>
		</div>
		<div class="video-block">
			<h2>Luxe, <br />nouvel age d'or</h2>
			<a class="btn-play" href="#">
				<span class="video-thumb-holder">
					<span class="btn-play"></span>
					<img src="./tmp/thumb-video.jpg" alt="thumb" width="178" height="100" />
					<span class="video-thumb-overlay">Voir la vidéo</span>
				</span>
			</a>
		</div>
		<div class="video-block">
			<h2>Temple <br />de la consommation</h2>
			<a href="#">
				<span class="video-thumb-holder">
					<span class="btn-play"></span>
					<img src="./tmp/thumb-video.jpg" alt="thumb" width="178" height="100" />
					<span class="video-thumb-overlay">Voir la vidéo</span>
				</span>
			</a>
		</div>
	</div>
</section>-->

<aside class="featured">
	<div class="container">
		<div class="extra-text">
			<span class="bg-left"></span>
			<p>«Faire comme il faut,
au moment adéquat» ?</p>
			<span class="author">Confucius*</span>
			<span class="bg-right"></span>
		</div>
		<div class="featured-content">
			<p>L’équilibre des forces est une recherche de tous les instants. Qu’il s’agisse de se nourrir, de se soigner ou de se positionner dans l’espace, le concept de Yin et Yang est présent dans tous les actes, des plus quotidiens aux plus exceptionnels.</p>
		</div>
	</div>
</aside>

<?php //include('./footer.php'); ?>
</div><!-- // page -->
<footer id="footer">
	<div id="sponsor">
		<span class="france24">Avec le soutien de FRANCE 24</span>
	</div>
	<nav id="footer-nav">
		<ul id="footer-menu">
			<li><a href="#" class="highlight">Menu</a></li>
			<li><a href="#">Trombinoscope</a></li>
			<li><a href="#">Bibliographie</a></li>
			<li><a href="#">Lexique</a></li>
			<li><a href="#">Contribuez</a></li>
			<li><a href="#">Credits</a></li>
		</ul>
	</nav>
	<div class="share">
		<iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fdev.tn07.net%2Fplongee-au-coeur-de-la-chine%2F&amp;send=false&amp;layout=button_count&amp;width=100&amp;show_faces=false&amp;font&amp;colorscheme=dark&amp;action=like&amp;height=21&amp;appId=638699199490003" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100px; height:21px;" allowTransparency="true"></iframe>
		<a href="https://twitter.com/share" class="twitter-share-button" data-url="http://dev.tn07.net/plongee-au-coeur-de-la-chine/" data-text="Plongée au cœur de la Chine - WebDocumentaire" data-hashtags="webdoc">Tweet</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
	</div>
</footer>
<div class="video-holder"></div>
<div class="background-holder installer"></div>
</body>
</html>