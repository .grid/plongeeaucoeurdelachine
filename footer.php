</div><!-- // page -->
<footer id="footer">
<?php
	if( !is_home() && !is_page('biographies') && !is_page('bibliographie') && !is_page('lexique') && !is_page('credits') ) {
?>

	<div class="sponsor">
		<span class="france24">Avec le soutien de FRANCE 24</span>
	</div>
	
	<?php

	$footerMenu = array(
		'theme_location'  => '',
		'menu'            => 'footer',
		'container'       => 'nav',
		'container_class' => '',
		'container_id'    => 'footer-nav',
		'menu_class'      => '',
		'menu_id'         => 'footer-menu',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
		'depth'           => 0,
		//'walker'          => new description_walker
	);
	
	wp_nav_menu( $footerMenu );
	?>
	<div class="share">
		<iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.plongeeaucoeurdelachine.com%2F&amp;send=false&amp;layout=button_count&amp;width=100&amp;show_faces=false&amp;font&amp;colorscheme=light&amp;action=like&amp;height=21&amp;appId=638699199490003" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100px; height:21px;" allowTransparency="true"></iframe>
		<a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.plongeeaucoeurdelachine.com/" data-text="Plongée au cœur de la Chine" data-hashtags="webdocu">Tweet</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
	</div>
	<?php
	} elseif( is_page('biographies') || is_page('bibliographie') || is_page('lexique') || is_page('credits') ){
		//show nothing inside the popin !
	} else {
	?>
	
	<div class="sponsor home">
		<span class="france24">Avec le soutien de FRANCE 24</span>
	</div>
	
	<div class="share">
		<iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.plongeeaucoeurdelachine.com%2F&amp;send=false&amp;layout=button_count&amp;width=100&amp;show_faces=false&amp;font&amp;colorscheme=light&amp;action=like&amp;height=21&amp;appId=638699199490003" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100px; height:21px;" allowTransparency="true"></iframe>
		<a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.plongeeaucoeurdelachine.com/" data-text="Plongée au cœur de la Chine" data-hashtags="webdocu">Tweet</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
	</div>
	<?php } ?>
	
</footer>

<?php get_template_part('tpl', 'video-player'); ?>

<?php
	global $post;
	
	if( is_home() || is_404() ){
		$parent_page_slug = 'intro';
	} elseif( 'cpt-trombinoscope' == get_post_type() || is_tax( 'tax-personality-index' ) || 'cpt-lexique' == get_post_type() || is_tax( 'tax-lexique-index' ) ){
		$parent_page_slug = 'biographies';
	} elseif ( $post->post_parent ) {
		$parent_slug = get_post($post->post_parent);
		$parent_page_slug = $parent_slug->post_name;
	} else {
		$parent_page_slug = $post->post_name;
	}
?>
<div class="page-wrapper <?php echo $parent_page_slug; ?>"></div>
<div class="background-holder <?php echo $parent_page_slug; ?>"></div>
<?php wp_footer(); ?>

</body>
</html>