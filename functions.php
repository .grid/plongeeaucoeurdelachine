<?php

add_theme_support( 'post-thumbnails' );

function webdoc_menus() {
  register_nav_menus(
    array(
      'main-menu' => __( 'Menu principal' ),
      'footer-menu' => __( 'Footer' )
    )
  );
}
add_action( 'init', 'webdoc_menus' );

/* THUMBS */
add_image_size('thumb-sequence', '178', '100', false);
add_image_size('thumb-author', '180', '180', false);
add_image_size('thumb-story', '99', '99', false);

// CUSTOM MENU WALKER
class description_walker extends Walker_Nav_Menu {
  private $chapNumber = 1;
  function start_el(&$output, $item, $depth, $args)
  {
       global $wp_query;
       $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

       $class_names = $value = '';

       $classes = empty( $item->classes ) ? array() : (array) $item->classes;

       $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
       $class_names = ' class="'. esc_attr( $class_names ) . '"';

       $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';

       $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
       $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
       $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
       $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

       
       //$description  = ! empty( $item->description ) ? '<div class="sous_menu"><h2 class="block_txt">'.esc_attr( $item->description ).'</h2></div>' : '';

       if($depth != 0)
       {
                 $description = "";
       }

        /*$item_output = $args->before;
        $item_output .= '<a'. $attributes .'>';
        $item_output .= $args->link_before . $this->chapNumber++ .apply_filters( 'the_title', $item->title, $item->ID );
        $item_output .= $args->link_after;
        $item_output .= '</a>'. $description;
        $item_output .= $args->after;*/
        
        $pos = strpos($class_names, "sub-menu");

		if ($pos === false) {    
		$item_output = $args->before
		. "<a $attributes>"
		. $args->link_before
		. apply_filters( 'the_title', $item->title, $item->ID )
		. '</a>'
		. $args->link_after
		. $description
		. $args->after;
		} else {
		$item_output = $args->before
		. "<a $attributes>"
		. $args->link_before . $this->chapNumber++
		. apply_filters( 'the_title', $item->title, $item->ID )
		. '</a>'
		. $args->link_after
		. $description
		. $args->after;
		};

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
        }
}

function showPortraits(){
	
	$argsPortraits = array('post_type' => 'cpt-trombinoscope', 'order' => 'ASC', 'posts_per_page' => -1);
	
	$queryPortraits = new WP_Query( $argsPortraits );
	
	if( $queryPortraits->have_posts() ):
	
		while($queryPortraits->have_posts()): $queryPortraits->the_post();
			
			if( get_field('acf_personality_story_thumb', get_the_ID() ) ){ 
				//$thumbLink = get_field('acf_personality_story_thumb'); 
				$personalityThumbID = get_field('acf_personality_story_thumb');
				$sizeThumb = 'thumb-story';
				
				$personalityThumb = wp_get_attachment_image_src( $personalityThumbID, $sizeThumb );
				//var_dump($chapThumb);
				
				if( get_field('acf_personality_story_video', get_the_ID()) ){
					$videoLinkStory = get_field('acf_personality_story_video');
				};
				
				echo "<a href='#' data-video='" . $videoLinkStory . "' class='story'><span class='featured-personnality'><span>" . get_the_title() . "</span><img src='" . $personalityThumb[0] . "' /></span></a>";
			} else{
				echo "<span class='featured-personnality'><span>" . get_the_title() . "</span></span>";
			};
			//if( get_field('acf_personality_story_video') ){ $videoLink = get_field('acf_personality_story_thumb'); };
			
			?>
			<!--<div class="hidden-player">
				<section id="video-player">
					<span class="btn-close">fermer</span>
					
					<?php 
						
					?>
					
					<div id="player">
						<?php //echo $videoLinkStory; ?>
						<div class="share video">
							<iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fdev.tn07.net%2Fplongee-au-coeur-de-la-chine%2F&amp;send=false&amp;layout=button_count&amp;width=100&amp;show_faces=false&amp;font&amp;colorscheme=dark&amp;action=like&amp;height=21&amp;appId=638699199490003" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100px; height:21px;" allowTransparency="true"></iframe>
							<iframe allowtransparency="true" frameborder="0" scrolling="no"
				            src="http://platform.twitter.com/widgets/tweet_button.html?url=http%3A%2F%2Fdev.tn07.net%2Fplongee-au-coeur-de-la-chine%2Ftweet_button&amp;count=horizontal;&amp;hashtags=webdoc"
				            style="width:80px; height:20px;"></iframe>
						</div>
					</div>
					<!--<aside><h2><?php //the_sub_field('acf_sequence_title') ?></h2><?php //echo get_sub_field('acf_sequence_description') ?></aside>-->
				<!--</section>
			</div>-->
			
			
		<?php	
		endwhile;
	
	endif;
	wp_reset_postdata();
}

/** POST TYPES **/
add_action( 'init', 'register_chapitres', 10 );
function register_chapitres() {
register_post_type( "cpt-chapitres", array (
  'labels' => 
  array (
    'name' => 'Chapitres',
    'singular_name' => 'Chapitre',
    'add_new' => 'Ajouter',
    'add_new_item' => 'Ajouter un nouveau chapitre',
    'edit_item' => 'Modifier le chapitre',
    'new_item' => 'Nouveau chapitre',
    'view_item' => 'Voir le chapitre',
    'search_items' => 'Chercher un chapitre',
    'not_found' => 'Aucun chapitre trouvé',
    'not_found_in_trash' => 'Aucun chapitre trouvé dans la corbeille',
    'parent_item_colon' => 'Chapitre parent:',
  ),
  'description' => '',
  'publicly_queryable' => true,
  'exclude_from_search' => false,
  'map_meta_cap' => true,
  'capability_type' => 'page',
  'public' => true,
  'hierarchical' => true,
  'rewrite' => 
  array (
    'slug' => 'chapitre',
    'with_front' => true,
    'pages' => true,
    'feeds' => false,
  ),
  'has_archive' => false,
  'query_var' => 'chapitre',
  'supports' => 
  array (
    0 => 'title',
    1 => 'editor',
    2 => 'thumbnail',
    3 => 'excerpt',
    4 => 'custom-fields',
    5 => 'page-attributes',
  ),
  'taxonomies' => 
  array (
  ),
  'show_ui' => true,
  'menu_position' => 30,
  'menu_icon' => false,
  'can_export' => true,
  'show_in_nav_menus' => true,
  'show_in_menu' => true,
) );
}

add_action( 'init', 'register_lexique', 10 );
function register_lexique() {
register_post_type( "cpt-lexique", array (
  'labels' => 
  array (
    'name' => 'Lexique',
    'singular_name' => 'Définition',
    'add_new' => 'Ajouter',
    'add_new_item' => 'Ajouter une nouvelle définition',
    'edit_item' => 'Modifier la définition',
    'new_item' => 'Nouvelle définition',
    'view_item' => 'Voir la définition',
    'search_items' => 'Chercher une définition',
    'not_found' => 'Aucune définition trouvée',
    'not_found_in_trash' => 'Aucune définition trouvée dans la corbeille',
    'parent_item_colon' => 'Entrée parente:',
  ),
  'description' => '',
  'publicly_queryable' => true,
  'exclude_from_search' => false,
  'map_meta_cap' => true,
  'capability_type' => 'post',
  'public' => true,
  'hierarchical' => false,
  'rewrite' => 
  array (
    'slug' => 'cpt-lexique',
    'with_front' => true,
    'pages' => true,
    'feeds' => true,
  ),
  'has_archive' => true,
  'query_var' => 'cpt-lexique',
  'supports' => 
  array (
    0 => 'title',
    1 => 'editor',
    2 => 'custom-fields',
  ),
  'taxonomies' => 
  array (
  ),
  'show_ui' => true,
  'menu_position' => 30,
  'menu_icon' => false,
  'can_export' => true,
  'show_in_nav_menus' => true,
  'show_in_menu' => true,
) );
}

add_action( 'init', 'register_story', 10 );
function register_story() {
register_post_type( "cpt-story", array (
  'labels' => 
  array (
    'name' => 'Histoires',
    'singular_name' => 'Histoire',
    'add_new' => 'Ajouter',
    'add_new_item' => 'Ajouter une nouvelle histoire',
    'edit_item' => 'Modifier l\'histoire',
    'new_item' => 'Nouvelle histoire',
    'view_item' => 'Voir l\'histoire',
    'search_items' => 'Chercher une histoire',
    'not_found' => 'Aucune histoire trouvée',
    'not_found_in_trash' => 'Aucune histoire trouvée dans la corbeille',
    'parent_item_colon' => 'Entrée parente:',
  ),
  'description' => '',
  'publicly_queryable' => true,
  'exclude_from_search' => false,
  'map_meta_cap' => true,
  'capability_type' => 'post',
  'public' => true,
  'hierarchical' => false,
  'rewrite' => 
  array (
    'slug' => 'histoire',
    'with_front' => true,
    'pages' => true,
    'feeds' => 'histoires',
  ),
  'has_archive' => 'histoires',
  'query_var' => 'histoire',
  'supports' => 
  array (
    0 => 'title',
    1 => 'custom-fields',
  ),
  'taxonomies' => 
  array (
  ),
  'show_ui' => true,
  'menu_position' => 30,
  'menu_icon' => false,
  'can_export' => true,
  'show_in_nav_menus' => true,
  'show_in_menu' => true,
) );
}

add_action( 'init', 'register_trombinoscope', 10 );
function register_trombinoscope() {
register_post_type( "cpt-trombinoscope", array (
  'labels' => 
  array (
    'name' => 'Biographies',
    'singular_name' => 'Personnalité',
    'add_new' => 'Ajouter',
    'add_new_item' => 'Ajouter une nouvelle personnalité',
    'edit_item' => 'Modifier la personnalité',
    'new_item' => 'Nouvelle personnalité',
    'view_item' => 'Voir la personnalité',
    'search_items' => 'Chercher une personnalité',
    'not_found' => 'Aucune personnalité trouvée',
    'not_found_in_trash' => 'Aucune personnalité trouvée dans la corbeille',
    'parent_item_colon' => 'Entrée parente:',
  ),
  'description' => '',
  'publicly_queryable' => true,
  'exclude_from_search' => false,
  'map_meta_cap' => true,
  'capability_type' => 'post',
  'public' => true,
  'hierarchical' => false,
  'rewrite' => 
  array (
    'slug' => 'biographie',
    'with_front' => true,
    'pages' => true,
    'feeds' => true,
  ),
  'has_archive' => true,
  'query_var' => 'biographie',
  'supports' => 
  array (
    0 => 'title',
    1 => 'editor',
    2 => 'thumbnail',
    3 => 'custom-fields',
  ),
  'taxonomies' => 
  array (
  ),
  'show_ui' => true,
  'menu_position' => 30,
  'menu_icon' => false,
  'can_export' => true,
  'show_in_nav_menus' => true,
  'show_in_menu' => true,
) );
}


/** TAXONOMIES **/
function register_personality() {

	$labels = array(
		'name'                       => 'Catégories',
		'singular_name'              => 'Catégorie',
		'menu_name'                  => 'Catégories',
		'all_items'                  => 'All Items',
		'parent_item'                => 'Parent Item',
		'parent_item_colon'          => 'Parent Item:',
		'new_item_name'              => 'New Item Name',
		'add_new_item'               => 'Add New Item',
		'edit_item'                  => 'Edit Item',
		'update_item'                => 'Update Item',
		'view_item'                  => 'View Item',
		'separate_items_with_commas' => 'Separate items with commas',
		'add_or_remove_items'        => 'Add or remove items',
		'choose_from_most_used'      => 'Choose from the most used',
		'popular_items'              => 'Popular Items',
		'search_items'               => 'Search Items',
		'not_found'                  => 'Not Found',
		'no_terms'                   => 'No items',
		'items_list'                 => 'Items list',
		'items_list_navigation'      => 'Items list navigation',
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'tax-personality-index', array( 'cpt-trombinoscope' ), $args );

}
add_action( 'init', 'register_personality', 0 );


// Register Custom Taxonomy
function register_lexique_index() {

	$labels = array(
		'name'                       => 'Termes',
		'singular_name'              => 'Terme',
		'menu_name'                  => 'Termes',
		'all_items'                  => 'All Items',
		'parent_item'                => 'Parent Item',
		'parent_item_colon'          => 'Parent Item:',
		'new_item_name'              => 'New Item Name',
		'add_new_item'               => 'Add New Item',
		'edit_item'                  => 'Edit Item',
		'update_item'                => 'Update Item',
		'view_item'                  => 'View Item',
		'separate_items_with_commas' => 'Separate items with commas',
		'add_or_remove_items'        => 'Add or remove items',
		'choose_from_most_used'      => 'Choose from the most used',
		'popular_items'              => 'Popular Items',
		'search_items'               => 'Search Items',
		'not_found'                  => 'Not Found',
		'no_terms'                   => 'No items',
		'items_list'                 => 'Items list',
		'items_list_navigation'      => 'Items list navigation',
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'tax-lexique-index', array( 'cpt-lexique' ), $args );

}
add_action( 'init', 'register_lexique_index', 0 );

/* ENQUEUED SCRIPTS & STYLES */
function webdoc_scripts() {
  
  /* Register a custom version of jQuery from Google CDN, plus the jquery-migrate.js plugin for enhanced compatibility with jQuery 1.9/2.0 & older JS libraries */
  if( !is_admin() ){
    wp_deregister_script('jquery');
    wp_register_script('jquery', ("https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"), array(), '1.8.1', false);
    wp_enqueue_script('jquery');
    wp_register_script('jquery-migrate', ("http://code.jquery.com/jquery-migrate-1.2.1.min.js"), array(), '1.2.1', false);
    wp_enqueue_script('jquery-migrate');
  }
      
  /* Enqueue our main theme's style.css file */
  wp_enqueue_style( 'webdoc', get_stylesheet_uri(), array(), '1.0.0', 'screen' );
  wp_enqueue_style( 'zoombox', get_template_directory_uri() . '/css/zoombox.min.css', array('webdoc'), '1.0.0', 'screen' );

  /* Register then Enqueue all scripts */
  /* Enqueue our main theme's scripts.js file */
  wp_register_script( 'webdoc', get_template_directory_uri() . '/js/webdoc.min.js', array( 'jquery' ), '1.0', true );
  wp_enqueue_script( 'webdoc' );

  /* Zoombox */
  wp_register_script( 'zoombox', get_template_directory_uri() . '/js/zoombox.min.js', array('jquery'), '1.0', true );
  wp_enqueue_script( 'zoombox' );

  wp_register_script( 'player', get_template_directory_uri() . '/js/chapter_marker_player.min.js', array('jquery'), '1.0', true );
  wp_enqueue_script( 'player' );
  
}
add_action( 'wp_enqueue_scripts', 'webdoc_scripts' );