<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<title><?php wp_title(); ?></title>

	<?php wp_head(); ?>
	<?php if( is_tax( 'tax-personality-index' ) ){ ?>
		<script type="text/javascript">
			$(document).ready(function(){
				var panelPersonality = $('.block-personality');
				panelPersonality.hide();
				
				var url = document.URL.split('#')[1];
				if( url != undefined ){
					$('.other-personalities ul li a[href="'+url+'"]').addClass('active');
					//alert(url);
					$('.block-personality.'+ url).show();
				} else {
					$('.other-personalities ul li a').first().addClass('active');
					$('.block-personality').last().show();
				};
				
				
				
				$('.other-personalities ul li a').on('click', function(e){
					e.preventDefault();
					
					var hash = $(this).attr('href');
					location.hash = hash;
					
					if( $(this).hasClass('active')){
						return false;
					}else{
						$('.other-personalities ul li a').removeClass('active');
						var showPersonality = $(this).attr('class');
						$(this).addClass('active');
						//$(this).parent();
						panelPersonality.hide();
						$('.block-personality.'+ showPersonality).fadeIn(400);
					}
				});
			});
		</script>
		<?php } ?>
		
		<?php if( is_tax( 'tax-lexique-index' ) ){ ?>
		<script type="text/javascript">
			$(document).ready(function(){
			var panelPersonality = $('.block-definition');
			panelPersonality.hide();
			
			var url = document.URL.split('#')[1];
			if( url != undefined ){
				$('.other-definitions ul li a[href="'+url+'"]').addClass('active');
				//alert(url);
				$('.block-definition.'+ url).show();
			} else {
				$('.other-definitions ul li a').first().addClass('active');
				$('.block-definition').last().show();
			}
			
			
			$('.other-definitions ul li a').on('click', function(e){
				e.preventDefault();
				
				var hash = $(this).attr('href');
				location.hash = hash;
				
				if( $(this).hasClass('active')){
					return false;
				}else{
					$('.other-definitions ul li a').removeClass('active');
					var showPersonality = $(this).attr('class');
					$(this).addClass('active');
					//$(this).parent();
					panelPersonality.hide();
					$('.block-definition.'+ showPersonality).fadeIn(400);
				}
			});
		
			});
		</script>
		<?php } ?>
</head>
<?php
	if( is_home() ){
		$parent_page_slug = 'home';
	} elseif( 'cpt-trombinoscope' == get_post_type() || is_tax( 'tax-personality-index' ) || 'cpt-lexique' == get_post_type() || is_tax( 'tax-lexique-index' ) ){
		$parent_page_slug = 'biographies';
	}else {
		$parent_slug = get_post($post->post_parent);
		$parent_page_slug = $parent_slug->post_name;
	}
?>
<body class="<?php echo $parent_page_slug; ?>">
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-41483421-1', 'auto');
  ga('send', 'pageview');

</script>
<div class="wrapper-page <?php echo $parent_page_slug; ?>">
	<header id="header">
		<div class="container">
		<?php
			
			get_template_part('tpl', 'custom-menu');
			
		?>
		</div>
	</header>