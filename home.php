<?php get_header(); ?>

<span class="logo-hd"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-webdoc-chine.png" al="Plongée au cœur de la Chine" /></span>

<section class="chapter videos">
	<div class="container">
		<?php 
			$postIntroID = '353';
			while(has_sub_field('acf_sequence', $postIntroID)): 
		?>
		<div class="block-intro webdoc-complete">
			<?php
				$videoLinkTeaser = get_sub_field('acf_sequence_video', $postIntroID );
				$videoTitleTeaser = get_sub_field( 'acf_sequence_title' , $postIntroID );
				$videoDescriptionTeaser = get_sub_field( 'acf_sequence_description' , $postIntroID );
			?>
			<h2><a class="video teaser" href="#" data-video='<?php echo htmlentities( $videoLinkTeaser ); ?>' data-video-title='<?php echo $videoTitleTeaser; ?>' data-video-description='<?php echo htmlentities( $videoDescriptionTeaser ); ?>'><?php echo htmlentities( $videoTitleTeaser ); ?></a></h2>
			<a class="video teaser" href="#" data-video='<?php echo htmlentities( $videoLinkTeaser ); ?>' data-video-title='<?php echo htmlentities( $videoTitleTeaser ); ?>' data-video-description="<?php echo $videoDescriptionTeaser; ?>">
				<span class="video-thumb-holder">
					<span class="btn-play"></span>
					<?php echo get_the_post_thumbnail($postIntroID); ?>
					<span class="video-thumb-overlay"><?php _e('Voir la vidéo', 'webdoc'); ?></span>
				</span>
			</a>
		</div>
		<?php endwhile; ?>
		<div class="block-intro webdoc-interactive">
			<h2><a href="<?php echo get_permalink( 11 ); ?>">Plongez au <br />cœur de la Chine</a></h2>
			<span>Version interactive</span>
		</div>
	</div>
</section>
 

<aside class="featured">
	<div class="container">
		<div class="featured-content">
			<p>Chine de Confucius, Chine impériale, Chine de Mao, Chine d'aujourd'hui, Chine multiple avec ses 57 ethnies, ses 1,3 milliard d'habitants... <br />Que vais-je découvrir, moi l'étranger, en allant à la rencontre de ceux qui par leur réflexion, leur expérience ou leur histoire vont me donner quelques <br />clefs de compréhension de cette culture si différente et si particulière...</p>
		</div>
	</div>
</aside>

<?php get_footer(); ?>