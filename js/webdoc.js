$(document).ready(function(){
	$(".background-holder, span.logo-hd").css({
		'opacity' : '0'
	});

	$("ul.sub-menu").not('.regards-croises').each(function(){
		$i = 0;
		$j = new Array('0', 'I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X');
		$(this).find('li a').each( function(){
			$i++;
			$(this).prepend('<span class="chapter" title="Chapitre '+ $i +'">'+ $i +'</span>');
			$(this).append('<span>Chapitre ' + $j[$i] +'</span>');
		});
	});
	
	
	$('.video-block a').on('click', function(){		
			$("#player").append("<div id='player-fade'></div>");
			//$("iframe").hide();
			$("#iframe").load(function(){
				$('#player-fade').fadeOut(3000);
			});
			
			$("#video-player span.btn-close").on('click', function(){
				$(".video-holder").empty().fadeOut();
			});
			$(document).keyup(function(e) {
				//if (e.keyCode == 13) { $('.save').click(); }     // enter
				if (e.keyCode == 27) { $(".video-holder").empty().fadeOut(); }   // esc
			});
		/*});*/
		return false;
	});
	
	$('.video-block a, a.video.teaser, .portraits-list a, ul.sub-menu.regards-croises a').on('click', function(e){
			
		e.preventDefault();
		
		var Player = $(this).data('video');
		var Title = $(this).data('video-title');
		var Description = $(this).data('video-description');
		//var AdditionalContent = $(this).data('video-additionalcontent');
		var personalitiesList = $(this).data('video-authors');
		var lexiqueList = $(this).data('video-lexique');
		var playerFull = $(this).data('video-chapters');
		
		var videoContainer = $('.video-wrapper').find('#player');
		
		if( playerFull !== undefined && playerFull !== '' ){
			//alert(playerFull);
			videoContainer.empty().append(playerFull);
			videoContainer.parent().parent().find('aside').empty();
			videoContainer.parent().parent().find('aside').append('<h2>'+ Title +'</h2>');
			videoContainer.parent().parent().find('aside').append(Description);
			videoContainer.parent().parent().find('aside').append(personalitiesList);
			videoContainer.parent().parent().find('aside').append(lexiqueList);
		} else {
			videoContainer.empty().append(Player);
			videoContainer.parent().parent().find('aside').empty();
			videoContainer.parent().parent().find('aside').append('<h2>'+ Title +'</h2>');
			videoContainer.parent().parent().find('aside').append(Description);
			//videoContainer.parent().parent().find('aside').append(AdditionalContent);
			videoContainer.parent().parent().find('aside').append(personalitiesList);
			videoContainer.parent().parent().find('aside').append(lexiqueList);
		};
		
		
		/*if( $(this).hasClass('story') ){
			//videoContainer.parent().parent().find('aside').empty();
			videoContainer.parent().parent().find('aside').empty();
		}*/
		
		//alert(Title);
		
		$('#video-player .additional-links a.popin, #video-player .additional-definitions a.popin').on('click', function(e){
			e.preventDefault();
			var link = $(this).attr('href');
			var options = {
			    width: 1024,
			    height: 700
			};
			$.zoombox.open(link,options);
		});
		
		$('.video-wrapper, .video-wrapper .hidden-player').fadeIn();
		$("#iframe").load(function(){
			$('#player-fade').fadeOut(3000);
		});
		
		$("#video-player span.btn-close").on('click', function(){
			$(".video-wrapper").fadeOut(700, function(){
				$(this).find("#player").empty();
			});
		});
		$(document).keyup(function(e) {
			if (e.keyCode == 27) {
				$(".video-wrapper").fadeOut(700, function(){
					$(this).find("#player").empty();
				});
			}
		});
	});
	
	var numberOfStories = $('.featured-personnality').length;
	$('.featured-content.authors .portraits-list div').css({
		'width' : '104' * (numberOfStories)
	});
	
	var firstSlideAuthor = $('div.portraits-list a').first();
	firstSlideAuthor.addClass('first');
	
	var lastSlideAuthor = $('div.portraits-list a').last();
	lastSlideAuthor.addClass('last');
	
	var click=0;
	$('a.btn-next-personnalities').on('click', function(e){
		//alert('ok');
		
		//alert(numberOfStories);
		
		click++;
		e.preventDefault();

		$("div.portraits-list div").stop(true, false, false).animate({marginLeft:-104},300,function(){
			$(this).css({marginLeft:0}).find("a:last").after($(this).find("a:first"));
		})
	});
	
	$('#video-player span.youtube-hd').hover(function(){
		$('#video-player span.youtube-hd-message').fadeIn();
	}, function(){
		$('#video-player span.youtube-hd-message').fadeOut();
	});
	
});

$(window).load(function(){
	$(".background-holder").animate({
		'opacity' : '1'
	}, '900', 'linear');

	$("span.logo-hd").animate({
		'opacity' : '1'
	}, '1500', 'linear');
});

$(function(){
	
	$('li.popin a').zoombox({
		theme       : 'zoombox',        //available themes : zoombox,lightbox, prettyphoto, darkprettyphoto, simple
		opacity     : 0.8,              // Black overlay opacity
		duration    : 800,              // Animation duration
		animation   : true,             // Do we have to animate the box ?
		width       : 995,              // Default width
		height      : 700,              // Default height
		//gallery     : true,             // Allow gallery thumb view
		//autoplay : false                // Autoplay for video
	});
	
});