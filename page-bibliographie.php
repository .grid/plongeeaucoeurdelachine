<?php get_header(); ?>
<?php
	
	$argsAuthors = array('post_type' => 'cpt-trombinoscope', 'posts_per_page' => '-1', 'meta_key' => 'acf_personnalites_bibliographie', 'meta_value' => ' ', 'meta_compare' => '!=' );
	
	$queryAuthors = new WP_Query( $argsAuthors );
	
	//$queried_object2 = get_queried_object();
	//$termName = $queried_object->slug;
	//var_dump($queried_object2);
?>
<?php if($queryAuthors->have_posts()): ?>
	<div class="biographies-index">
	<?php
		$args = array( 'taxonomy' => 'tax-personality-index', 'hide_empty' => false );
		$terms = get_terms('tax-personality-index', $args);
	
		$count = count($terms);
			echo '<ul>';
			$term_list = '';
			foreach ($terms as $term) {
				$term_list .= '<li>';
				if( $term->count != 0 ){
					$term_list .= '<a href="./' . $term->slug . '/" title="' . $term->name . '" class="index-link ' . $term->slug . '">' . $term->name . '</a>';
				} else {
					$term_list .= $term->name;
				}
				$term_list .= '</li>';
			}
			echo $term_list;
			echo '</ul>';
	?>
	</div>
	
	<section class="biography">
		<div class="container personality">
			
			<?php while($queryAuthors->have_posts()): $queryAuthors->the_post(); ?>
				
				<?php
				$personnalityTaxo = get_the_terms( $post->ID, 'tax-personality-index' );
										
				if ( $personnalityTaxo && ! is_wp_error( $personnalityTaxo ) ) : 
				
					$personalityTaxoList = array();
				
					foreach ( $personnalityTaxo as $term ) {
						$personalityTaxoList[] = $term->slug;
					}
										
					$showPersonalityTaxo = join( " ", $personalityTaxoList );
				?>
				
				
				<?php endif; ?>
				
				<div class="block-personality <?php echo $showPersonalityTaxo; ?> <?php echo $post->post_name; ?>">
					<?php echo get_the_post_thumbnail(get_the_ID(), 'thumb-author', array('class' => 'thumb personality')); ?>
					<div class="description-personality">
						<h2><?php echo get_the_title(); ?></h2>
						<div class="personality content">
							<aside class="personality additionals-infos">
								<?php if( get_field('acf_personnalites_bibliographie') ) {
									the_field('acf_personnalites_bibliographie');
								} ?>
							</aside>
						</div>
					</div>
					
				</div>
				
			<?php endwhile; ?>
			<div class="other-personalities">
				<?php 
					//$queried_object = get_queried_object();
					$termName = 'a';
					
					//$argsTaxAuthors = array('taxonomy' => 'tax-personality-index' );
					
					$argsName = array('post_type' => 'cpt-trombinoscope'/*, 'tax_query' => array( $argsTaxAuthors )*/, 'orderby' => 'id', 'order' => 'ASC', 'posts_per_page' => '-1', 'meta_key' => 'acf_personnalites_bibliographie', 'meta_value' => ' ', 'meta_compare' => '!=' );
					$queryName = new WP_Query( $argsName );
					
					if($queryName->have_posts()): 
				?>
					<ul>
						<?php while($queryName->have_posts()): $queryName->the_post(); ?>
						
						<?php $personnalityTaxo = get_the_terms( $post->ID, 'tax-personality-index' );
													
							if ( $personnalityTaxo && ! is_wp_error( $personnalityTaxo ) ) : 
							
								$personalityTaxoList = array();
							
								foreach ( $personnalityTaxo as $term ) {
									$personalityTaxoList[] = $term->slug;
								}
													
								$showPersonalityTaxo = join( " ", $personalityTaxoList );
							?>
							
							
							<?php endif; ?>
						
						<li><a href="<?php echo $post->post_name; ?>" class="<?php echo $post->post_name; ?> <?php echo $showPersonalityTaxo; ?>"><?php echo get_the_title(); ?></a></li>
						<?php endwhile; ?>
					</ul>
					<?php endif; ?>
				<?php wp_reset_postdata(); ?>
			</div>
		</div>
	</section>
<?php endif; ?>
<script type="text/javascript">
	$(document).ready(function(){
		
		
		
				
		$('.block-personality').each(function(){
			//alert('result');
			var indexClasses = $(this).attr('class').split(/\s+/);
			var firstClass = indexClasses[1];
			//alert(indexClasses[1]);
			test(firstClass);
			
			//var indexClass = .hasClass( classList[1] ) )
		});
		
		
		function test(firstClass){
			$('a.index-link').each(function(){
			//alert(firstClass);
				if( $(this).hasClass( firstClass ) ){
					$(this).addClass('result');
				} else {
					$(this).addClass('nope');
				};
			});
		};
		
		$('a.index-link:not(.result)').contents().unwrap();

			
		$('.block-personality').each(function(){
			//alert('result');
			var indexClasses = $(this).attr('class').split(/\s+/);;
			
			//alert(indexClasses[1]);
			
			
			//var indexClass = .hasClass( classList[1] ) )
		});
		
		/*if( $('a.index-link').hasClass('nope') ){
			$(this).contents().unwrap();
		}*/
		
		//if ($("").hasClass($('#pagetitle').attr('class')))
		
		var panelPersonality = $('.block-personality');
		panelPersonality.hide();
		
		var linkPersonality = $('.other-personalities li a');
		linkPersonality.hide();
		
		/*var firstLinkPersonality = linkPersonality.first().attr('class');
		var classList = firstLinkPersonality.split(/\s+/);
		
		$('.block-personality.'+ classList[1]).show();
		
		$('.other-personalities li a.'+ classList[1]).show();*/
		
		$('a.index-link').on('click', function(e){
			e.preventDefault();
			
			$('a.index-link').removeClass('active');
			$(this).addClass('active');
			
			panelPersonality.hide();
			linkPersonality.hide();
			
			var classList = $(this).attr('class').split(/\s+/);
			if( $('.block-personality').hasClass( classList[1] ) ){
				$('.block-personality.'+ classList[1] ).addClass('active');
			  	$('.other-personalities li a.'+ classList[1] ).show();
			  	$('.other-personalities li a.'+ classList[1] ).first().addClass('active');
			  	
			  	//$('.block-personality.'+ classList[1] ).prevAll().addClass('active');
			  	
			  	$('.block-personality').not('.block-personality.'+ classList[1] ).removeClass('active');
			  	$('.other-personalities li a').not('.other-personalities li a.'+ classList[1] ).removeClass('active').hide();
			};
			
			//alert(classList[1]);
			//if( blockPersonality.hasClass('active') ){
			
				$('.block-personality.'+ classList[1] ).last().fadeIn(400);
				$('.other-personalities li a.'+ classList[1] ).parent().show();
			
			//} else {
				$('.block-personality').not('.block-personality.'+ classList[1] ).hide();
				$('.other-personalities li a').not('.other-personalities li a.'+ classList[1] ).parent().hide();
			//};
			
			//return false;
		});
		
		
		
		
		var url = document.URL.split('#')[1];
		if( url != undefined ){
			var requestedPersonality = $('.other-personalities li a[href="'+url+'"]');
			requestedPersonality.addClass('active');
			
			$('.block-personality.'+ url).show();
			$('.other-personalities li a.'+ requestedPersonalitiesIndex).show();
			
			var requestedPersonalityClasses = $('.block-personality.'+ url).attr('class').split(/\s+/);
			
			var requestedPersonalitiesIndex = requestedPersonalityClasses[1];
			$('.other-personalities ul li a.'+ requestedPersonalitiesIndex).show();
			
			//alert(requestedPersonalitiesIndex);
			
		} else {
			
			var firstPersonalitiesClasses = $('.block-personality').last().attr('class').split(/\s+/);
			
			var firstPersonalitiesClass = firstPersonalitiesClasses[1];
			
			//alert(firstPersonalitiesClass);
			
			$('.other-personalities ul li a.'+ firstPersonalitiesClass).first().addClass('active');
			$('.other-personalities ul li a.'+ firstPersonalitiesClass).show();
			$('.block-personality').last().show();
			
		}
		
		
		$('.other-personalities ul li a').on('click', function(e){
			e.preventDefault();
			
			var hash = $(this).attr('href');
			location.hash = hash;
			
			if( $(this).hasClass('active')){
				return false;
			}else{
				var classList = $(this).attr('class').split(/\s+/);
				var showPersonality = classList[0];
				$('.other-personalities ul li a').removeClass('active');
				$(this).addClass('active');
				//$(this).parent();
				$('.block-personality').hide();
				$('.block-personality.'+ showPersonality).fadeIn(400);
			}
		});
		
		
		
	});
</script>
<?php get_footer(); ?>