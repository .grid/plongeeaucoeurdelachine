<?php get_header(); ?>
<?php
	
	$argsAuthors = array('post_type' => 'cpt-trombinoscope', 'posts_per_page' => '-1', 'meta_key' => 'acf_personality_order', 'orderby' => 'meta_value_num', 'order' => 'DESC' );
	
	$queryAuthors = new WP_Query( $argsAuthors );
	
?>
<?php if($queryAuthors->have_posts()): ?>
	<div class="biographies-index">
	<?php
		$args = array( 'taxonomy' => 'tax-personality-index', 'hide_empty' => false );
		$terms = get_terms('tax-personality-index', $args);
	
		$count = count($terms);
			echo '<ul>';
			$term_list = '';
			foreach ($terms as $term) {
				$term_list .= '<li>';
				if( $term->count != 0 ){
					$term_list .= '<a href="./' . $term->slug . '/" title="' . $term->name . '" class="index-link ' . $term->slug . '">' . $term->name . '</a>';
				} else {
					$term_list .= $term->name;
				}
				$term_list .= '</li>';
			}
			echo $term_list;
			echo '</ul>';
	?>
	</div>
	
	<section class="biography">
		<div class="container personality">
			
			<?php while($queryAuthors->have_posts()): $queryAuthors->the_post(); ?>
				
				<?php
				$personnalityTaxo = get_the_terms( $post->ID, 'tax-personality-index' );
										
				if ( $personnalityTaxo && ! is_wp_error( $personnalityTaxo ) ) : 
				
					$personalityTaxoList = array();
				
					foreach ( $personnalityTaxo as $term ) {
						$personalityTaxoList[] = $term->slug;
					}
										
					$showPersonalityTaxo = join( " ", $personalityTaxoList );
				?>
				
				
				<?php endif; ?>
				
				<div class="block-personality <?php echo $showPersonalityTaxo; ?> <?php echo $post->post_name; ?>">
					<?php echo get_the_post_thumbnail(get_the_ID(), 'thumb-author', array('class' => 'thumb personality')); ?>
					<div class="description-personality">
						<h2><?php echo get_the_title(); ?></h2>
						<div class="personality content">
							<?php the_content(); ?>
							<aside class="personality additionals-infos">
								<?php if( get_field('acf_personnalites_functions') ) { ?>
								<h3>Titres</h3>
								<ul class="functions">
									<?php while(has_sub_field('acf_personnalites_functions')){ ?>
									<li><?php echo get_sub_field('acf_personnalites_function'); ?></li>
									<?php } ?>
								</ul>
								<?php } ?>
								<!--<h3>Distinctions</h3>
								<ul class="distinctions">
									<li><a href="#" target="_blank">Superasset Antiochum</a></li>
									<li><a href="#" target="_blank">Cato monstravit</a></li>
								</ul>-->
							</aside>
						</div>
					</div>
					
				</div>
			<?php endwhile; ?>
			<div class="other-personalities">
				<?php 
					
					$argsName = array('post_type' => 'cpt-trombinoscope'/*, 'tax_query' => array( $argsTaxAuthors )*/, 'posts_per_page' => '-1', 'meta_key' => 'acf_personality_order', 'orderby' => 'meta_value_num', 'order' => 'ASC' );
					$queryName = new WP_Query( $argsName );
					
					if($queryName->have_posts()): 
				?>
					<ul>
						<?php while($queryName->have_posts()): $queryName->the_post(); ?>
						
						<?php $personnalityTaxo = get_the_terms( $post->ID, 'tax-personality-index' );
													
							if ( $personnalityTaxo && ! is_wp_error( $personnalityTaxo ) ) : 
							
								$personalityTaxoList = array();
							
								foreach ( $personnalityTaxo as $term ) {
									$personalityTaxoList[] = $term->slug;
								}
													
								$showPersonalityTaxo = join( " ", $personalityTaxoList );
							?>
							
							
							<?php endif; ?>
						
						<li><a href="<?php echo $post->post_name; ?>" class="<?php echo $post->post_name; ?> <?php echo $showPersonalityTaxo; ?>"><?php echo get_the_title(); ?></a></li>
						<?php endwhile; ?>
					</ul>
					<?php endif; ?>
				<?php wp_reset_postdata(); ?>
			</div>
		</div>
	</section>
<?php endif; ?>
<script type="text/javascript">
	$(document).ready(function(){
		
		var panelPersonality = $('.block-personality');
		panelPersonality.hide();
		
		var linkPersonality = $('.other-personalities li a');
		linkPersonality.hide();
		
		$('a.index-link').on('click', function(e){
			e.preventDefault();
			
			$('a.index-link').removeClass('active');
			$(this).addClass('active');
			
			panelPersonality.hide();
			linkPersonality.hide();
			
			var classList = $(this).attr('class').split(/\s+/);
			if( $('.block-personality').hasClass( classList[1] ) ){
				$('.block-personality.'+ classList[1] ).addClass('active');
			  	$('.other-personalities li a.'+ classList[1] ).show();
			  	$('.other-personalities li a.'+ classList[1] ).first().addClass('active');
			  	
			  	//$('.block-personality.'+ classList[1] ).prevAll().addClass('active');
			  	
			  	$('.block-personality').not('.block-personality.'+ classList[1] ).removeClass('active');
			  	$('.other-personalities li a').not('.other-personalities li a.'+ classList[1] ).removeClass('active').hide();
			};
			
			//alert(classList[1]);
			//if( blockPersonality.hasClass('active') ){
			
				$('.block-personality.'+ classList[1] ).last().fadeIn(400);
				$('.other-personalities li a.'+ classList[1] ).parent().show();
			
			//} else {
				$('.block-personality').not('.block-personality.'+ classList[1] ).hide();
				$('.other-personalities li a').not('.other-personalities li a.'+ classList[1] ).parent().hide();
			//};
			
			//return false;
		});
		
		
		
		
		var url = document.URL.split('#')[1];
		if( url != undefined ){
			var requestedPersonality = $('.other-personalities li a[href="'+url+'"]');
			requestedPersonality.addClass('active');
			
			$('.block-personality.'+ url).show();
			$('.other-personalities li a.'+ requestedPersonalitiesIndex).show();
			
			var requestedPersonalityClasses = $('.block-personality.'+ url).attr('class').split(/\s+/);
			
			var requestedPersonalitiesIndex = requestedPersonalityClasses[1];
			$('.other-personalities ul li a.'+ requestedPersonalitiesIndex).show();
			
			
		} else {
			
			var firstPersonalitiesClasses = $('.block-personality').last().attr('class').split(/\s+/);
			
			var firstPersonalitiesClass = firstPersonalitiesClasses[1];
			
			$('.other-personalities ul li a.'+ firstPersonalitiesClass).first().addClass('active');
			$('.other-personalities ul li a.'+ firstPersonalitiesClass).show();
			$('.block-personality').last().show();
			
		}
		
		
		$('.other-personalities ul li a').on('click', function(e){
			e.preventDefault();
			
			var hash = $(this).attr('href');
			location.hash = hash;
			
			if( $(this).hasClass('active')){
				return false;
			}else{
				var classList = $(this).attr('class').split(/\s+/);
				var showPersonality = classList[0];
				$('.other-personalities ul li a').removeClass('active');
				$(this).addClass('active');
				$('.block-personality').hide();
				$('.block-personality.'+ showPersonality).fadeIn(400);
			}
		});
		
		
		
	});
</script>
<?php get_footer(); ?>