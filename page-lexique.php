<?php get_header(); ?>
<?php
	
	$argsAuthors = array('post_type' => 'cpt-lexique', 'posts_per_page' => '-1');
	
	$queryAuthors = new WP_Query( $argsAuthors );
	
	//$queried_object2 = get_queried_object();
	//$termName = $queried_object->slug;
	//var_dump($queried_object2);
?>
<?php if($queryAuthors->have_posts()): ?>
	<div class="lexique-index">
		<?php
		$args = array( 'taxonomy' => 'tax-lexique-index', 'hide_empty' => false );
		$terms = get_terms('tax-lexique-index', $args);
	
		$count = count($terms);
			echo '<ul>';
			$term_list = '';
			foreach ($terms as $term) {
				$term_list .= '<li>';
				if( $term->count != 0 ){
					$term_list .= '<a href="./' . $term->slug . '/" title="' . $term->name . '" class="lexique-link ' . $term->slug . '">' . $term->name . '</a>';
				} else {
					$term_list .= $term->name;
				}
				$term_list .= '</li>';
			}
			echo $term_list;
			echo '</ul>';
		?>
	</div>
	
	<section class="biography">
		<div class="container definition">
			
			<?php while($queryAuthors->have_posts()): $queryAuthors->the_post(); ?>
				
				<?php
				$personnalityTaxo = get_the_terms( $post->ID, 'tax-lexique-index' );
										
				if ( $personnalityTaxo && ! is_wp_error( $personnalityTaxo ) ) : 
				
					$personalityTaxoList = array();
				
					foreach ( $personnalityTaxo as $term ) {
						$personalityTaxoList[] = $term->slug;
					}
										
					$showPersonalityTaxo = join( " ", $personalityTaxoList );
				?>
				
				
				<?php endif; ?>
				
				<div class="block-definition <?php echo $showPersonalityTaxo; ?> <?php echo $post->post_name; ?>">
					<?php echo get_the_post_thumbnail(get_the_ID(), 'thumb-author', array('class' => 'thumb personality')); ?>
					<div class="description-definition">
						<h2><?php echo get_the_title(); ?></h2>
						<div class="definition content">
							<?php the_content(); ?>
						</div>
					</div>
					
				</div>
				
			<?php endwhile; ?>
			<div class="other-definitions">
				<?php 
					//$queried_object = get_queried_object();
					$termName = 'a';
					
					//$argsTaxAuthors = array('taxonomy' => 'tax-personality-index' );
					
					$argsName = array('post_type' => 'cpt-lexique'/*, 'tax_query' => array( $argsTaxAuthors )*/, 'orderby' => 'id', 'order' => 'ASC', 'posts_per_page' => '-1' );
					$queryName = new WP_Query( $argsName );
					
					if($queryName->have_posts()): 
				?>
					<ul>
						<?php while($queryName->have_posts()): $queryName->the_post(); ?>
						
						<?php $personnalityTaxo = get_the_terms( $post->ID, 'tax-lexique-index' );
													
							if ( $personnalityTaxo && ! is_wp_error( $personnalityTaxo ) ) : 
							
								$personalityTaxoList = array();
							
								foreach ( $personnalityTaxo as $term ) {
									$personalityTaxoList[] = $term->slug;
								}
													
								$showPersonalityTaxo = join( " ", $personalityTaxoList );
							?>
							
							
							<?php endif; ?>
						
						<li><a href="<?php echo $post->post_name; ?>" class="<?php echo $post->post_name; ?> <?php echo $showPersonalityTaxo; ?>"><?php echo get_the_title(); ?></a></li>
						<?php endwhile; ?>
					</ul>
					<?php endif; ?>
				<?php wp_reset_postdata(); ?>
			</div>
		</div>
	</section>
<?php endif; ?>
<script type="text/javascript">
	$(document).ready(function(){
		
		var panelPersonality = $('.block-definition');
		panelPersonality.hide();
		
		var linkPersonality = $('.other-definitions li a');
		linkPersonality.hide();
		
		/*var firstLinkPersonality = linkPersonality.first().attr('class');
		var classList = firstLinkPersonality.split(/\s+/);
		
		$('.block-personality.'+ classList[1]).show();
		
		$('.other-personalities li a.'+ classList[1]).show();*/
		
		$('a.lexique-link').on('click', function(e){
			e.preventDefault();
			
			$('a.lexique-link').removeClass('active');
			$(this).addClass('active');
			
			panelPersonality.hide();
			linkPersonality.hide();
			
			var classList = $(this).attr('class').split(/\s+/);
			if( $('.block-definition').hasClass( classList[1] ) ){
				$('.block-definition.'+ classList[1] ).addClass('active');
			  	$('.other-definitions li a.'+ classList[1] ).show();
			  	$('.other-definitions li a.'+ classList[1] ).first().addClass('active');
			  	
			  	//$('.block-personality.'+ classList[1] ).prevAll().addClass('active');
			  	
			  	$('.block-definition').not('.block-definition.'+ classList[1] ).removeClass('active');
			  	$('.other-definitions li a').not('.other-definitions li a.'+ classList[1] ).removeClass('active').hide();
			};
			
			//alert(classList[1]);
			//if( blockPersonality.hasClass('active') ){
			
				$('.block-definition.'+ classList[1] ).last().fadeIn(400);
				$('.other-definitions li a.'+ classList[1] ).parent().show();
			
			//} else {
				$('.block-definition').not('.block-definition.'+ classList[1] ).hide();
				$('.other-definitions li a').not('.other-definitions li a.'+ classList[1] ).parent().hide();
			//};
			
			//return false;
		});
		
		
		
		
		var url = document.URL.split('#')[1];
		if( url != undefined ){
			var requestedPersonality = $('.other-definitions li a[href="'+url+'"]');
			requestedPersonality.addClass('active');
			
			$('.block-definition.'+ url).show();
			$('.other-definitions li a.'+ requestedPersonalitiesIndex).show();
			
			var requestedPersonalityClasses = $('.block-definition.'+ url).attr('class').split(/\s+/);
			
			var requestedPersonalitiesIndex = requestedPersonalityClasses[1];
			$('.other-definitions ul li a.'+ requestedPersonalitiesIndex).show();
			
			//alert(requestedPersonalitiesIndex);
			
		} else {
			
			var firstPersonalitiesClasses = $('.block-definition').last().attr('class').split(/\s+/);
			
			var firstPersonalitiesClass = firstPersonalitiesClasses[1];
			
			//alert(firstPersonalitiesClass);
			
			$('.other-definitions ul li a.'+ firstPersonalitiesClass).first().addClass('active');
			$('.other-definitions ul li a.'+ firstPersonalitiesClass).show();
			$('.block-definition').last().show();
			
		}
		
		
		$('.other-definitions ul li a').on('click', function(e){
			e.preventDefault();
			
			var hash = $(this).attr('href');
			location.hash = hash;
			
			if( $(this).hasClass('active')){
				return false;
			}else{
				var classList = $(this).attr('class').split(/\s+/);
				var showPersonality = classList[0];
				$('.other-definitions ul li a').removeClass('active');
				$(this).addClass('active');
				//$(this).parent();
				$('.block-definition').hide();
				$('.block-definition.'+ showPersonality).fadeIn(400);
			}
		});
		
		
		
	});
</script>
<?php get_footer(); ?>