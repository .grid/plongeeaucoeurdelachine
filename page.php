<?php get_header(); ?>
<?php if(have_posts()): ?>
<section class="page">
	<?php while(have_posts()): the_post(); ?>
	<div class="container page">
		<?php the_content(); ?>
	</div>
	
	<?php endwhile; ?>
<?php endif; ?>
</section>
<?php get_footer(); ?>