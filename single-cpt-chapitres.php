<?php get_header(); ?>

<?php	
	global $post;
	
	if( get_field('acf_sequence') ) { 
?>
	<section class="chapter videos">
		<div class="container">
			<?php if ( get_field('acf_chapter_description') ) { 
				echo '<p class="chapter-description">' . get_field('acf_chapter_description') . '</p>';
			}; ?>
			
			<?php while(has_sub_field('acf_sequence')): ?>
			<div class="video-block">
				<h2><?php the_sub_field('acf_sequence_title'); ?></h2>
				<?php
					if( get_sub_field('acf_sequence_video', get_the_ID()) ){
						$videoLinkChapter = get_sub_field('acf_sequence_video');
					};
					if( get_sub_field('acf_sequence_title') ){
						$videoTitleChapter = get_sub_field('acf_sequence_title');
					};
					if( get_sub_field('acf_sequence_description') ){
						$videoDescriptionChapter = get_sub_field('acf_sequence_description');
					};
					
					if( get_sub_field('acf_sequence_personalities') ){
						$personnalitiesList = '';
						$personnalitiesList .= '<div class="additional-links">';
						
						while( has_sub_field('acf_sequence_personalities') ){
							
							$post_object = get_sub_field('acf_sequence_personality');
							if( $post_object ): 
								
								// override $post
								$post = $post_object;
								setup_postdata( $post ); 
								$label = '';
								
								//$i = '0';
								//while( $i >= '1' ){
									$label .= "Intervenant : ";
									
									/*while( $i >= '1' ) {
										$label .= "<sub>" . $i . "er</sub> Intervenant : ";
									}*/
								//};
								//$i++;
								//for ( ;  ; $i++) {
								    //if ($i == 1){
								    	//$label .= "<sub>" . $i . "er</sub> Intervenant : ";
								        //continue;
								        //}
								    
								//};
								
								//var_dump($post);
								
								//if ( $i > '1' ){ 
									
								//foreach ($post as &$value) {
									    
								//};
									
									
									
								/*} elseif( $i = '1' ){
									$label = "Intervenant : ";
								};*/
								
								$label = htmlspecialchars_decode(htmlentities( $label, ENT_NOQUOTES, 'UTF-8'));
								
								$authorTitle = get_the_title();
								$authorCleanTitle = htmlentities( $authorTitle, ENT_NOQUOTES, 'UTF-8');
								
								$personnalitiesList .= '<p><span class="title">' . $label . '</span>' . $authorCleanTitle . '</p><p><a href="' . home_url() . '/biographies/#' . $post->post_name . '" class="popin" >Biographie</a>';
								
								if( get_field('acf_personnalites_bibliographie') && get_field('acf_personnalites_bibliographie') != '' ){
											
											$personnalitiesList .= ' - <a href="' . home_url() . '/bibliographie/#' . $post->post_name . '" class="popin">Bibliographie</a></p>';
										
								};
								
								// - <a href="' . home_url() . '/bibliographie/#' . $post->post_name . '" class="popin" >Bibliographie</a></p>';
								
							    wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly
							endif;
							
							
						};
						$personnalitiesList .= '</div>';
						//wp_reset_postdata();
					} else{ $personnalitiesList =''; };
					
					if( get_sub_field('acf_sequence_definitions') ){
						$lexiqueList = '';
						$lexiqueList .= '<p class="additional-definitions"><span class="title">Lexique : </span>';
						while( has_sub_field('acf_sequence_definitions') ){
							
							$post_object = get_sub_field('acf_sequence_definition');
							if( $post_object ): 
							 
								// override $post
								$post = $post_object;
								setup_postdata( $post );
								
								$lexiqueList .= '<a class="popin" href="' . home_url() . '/lexique/#' . $post->post_name . '">' . get_the_title() . '</a> ';
								
							    wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly
							endif;
							
							
						};
						$lexiqueList .= '</p>';
						//wp_reset_postdata();
					} else{ $lexiqueList =''; };
					
					if( get_sub_field('acf_sequence_additionals') ){
						$videoAdditionnalContent = get_sub_field('acf_sequence_additionals');
					} else{ $videoAdditionnalContent = ''; };
					
				?>
				<?php
					$dataVideoChapters = '';
					if( get_sub_field('acf_sequence_video_id') && get_sub_field('acf_sequence_video_id') != ' ' ){
						
						$videoID = get_sub_field('acf_sequence_video_id');
						
						$dataVideoChapters .= "<script type='text/javascript'>
						ChapterMarkerPlayer.insert({
							container: 'player',
							videoId: '" . $videoID . "',
							width: '560',
							height: '315',";
						
						if( get_sub_field('acf_sequence_video_marker_timecodes') ){
							
							$dataVideoChapters .= "chapters: {";
							$dataVideoChaptersList = '';
							
							while( has_sub_field('acf_sequence_video_marker_timecodes') ){
								$markerTitle = get_sub_field('acf_sequence_video_marker_title');
								$markerTimecode = get_sub_field('acf_sequence_video_marker_timecode');
								
								
								
								$dataVideoChaptersList .= "'" . $markerTimecode . "': '" . $markerTitle . "', ";
								
							};
							
							
							
							$dataVideoChapters .= $dataVideoChaptersList . "}";
						};
						
						$dataVideoChapters .= "}); </script>";
						
						
						
					} else { $dataVideoChapters = ''; };
					//var_dump($dataVideoChapters);
					
					//$dataVideoChapters = htmlentities( $dataVideoChapters);
					
				?>
				
				
				<a href='#' class='chapter' data-video='<?php echo $videoLinkChapter; ?>' data-video-title="<?php echo $videoTitleChapter; ?>" data-video-description="<?php echo $videoDescriptionChapter; ?>" data-video-authors="<? echo htmlentities( $personnalitiesList ); ?>" data-video-lexique='<? echo $lexiqueList; ?>' data-video-chapters="<?php echo html_entity_decode( $dataVideoChapters ); ?>" >
					<span class="video-thumb-holder">
						<span class="btn-play"></span>
						<?php
							$chapThumbID = get_sub_field('acf_sequence_thumb');
							$sizeThumb = 'thumb-sequence'; // (thumbnail, medium, large, full or custom size)
							 
							$chapThumb = wp_get_attachment_image_src( $chapThumbID, $sizeThumb );
							//var_dump($chapThumb);
						?>
						<img src="<?php echo $chapThumb[0]; ?>" />
						<!--<img src="./tmp/thumb-video-2.jpg" alt="thumb" width="178" height="100" />-->
						<span class="video-thumb-overlay"><?php _e('Voir la vidéo', 'webdoc'); ?></span>
					</span>
				</a>
			</div>
			<?php endwhile; ?>
		</div>
	</section>
<?php }; ?>
<div id="mycontent"></div>
<?php
	global $post;
	if ( 'cpt-chapitres' == get_post_type() && $post->post_parent ) {   // test to see if the page has a parent
		//return $post->post_parent;             // return the ID of the parent post
		//echo 'ss-chap';
		get_template_part('tpl', 'bandeau-ss-chap');
	} else {
		//echo 'chap';
		get_template_part('tpl', 'bandeau-chap');
	}
?>
<script type="text/javascript" charset="utf-8">  
	/*jQuery(document).ready(function(){  
		jQuery('div.portraits-list a').on('click', function(e){  
			e.preventDefault();  
			var link = jQuery(this).attr('href');
			jQuery('#mycontent').html('Loading...');
			jQuery('#mycontent').load(link);
		});
	});*/
</script>
<?php get_footer(); ?>