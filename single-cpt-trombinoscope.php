<?php get_header(); ?>

<?php if(have_posts()): ?>
	<div class="biographies-index">
		<?php
		$args = array( 'taxonomy' => 'tax-personality-index', 'hide_empty' => false );
		$terms = get_terms('tax-personality-index', $args);
	
		$count = count($terms);
			echo '<ul>';
			$term_list = '';
			foreach ($terms as $term) {
				$term_list .= '<li>';
				if( $term->count != 0 ){
					$term_list .= '<a href="' . home_url() . '/biographies/' . $term->slug . '" title="' . sprintf(__('View all post filed under %s', 'my_localization_domain'), $term->name) . '">' . $term->name . '</a>';
				} else {
					$term_list .= $term->name;
				}
				$term_list .= '</li>';
			}
			echo $term_list;
			echo '</ul>';
		?>
	</div>
	
	<section class="biography">
		<div class="container personality">
			
			<?php while(have_posts()): the_post(); ?>
				
				<div class="block-personality <?php echo $post->post_name; ?>">
					<?php echo get_the_post_thumbnail(get_the_ID(), 'thumb-author', array('class' => 'thumb personality')); ?>
					<div class="description-personality">
						<h2><?php echo get_the_title(); ?></h2>
						<?php the_content(); ?>
						<aside class="personality additionals-infos">
							<?php if( get_field('acf_personnalites_functions') ) { ?>
							<h3>Titres</h3>
							<ul class="functions">
								<?php while(has_sub_field('acf_personnalites_functions')){ ?>
								<li><?php echo get_sub_field('acf_personnalites_function'); ?></li>
								<?php } ?>
							</ul>
							<?php } ?>
							<h3>Distinctions</h3>
							<ul class="distinctions">
								<li><a href="#" target="_blank">Superasset Antiochum</a></li>
								<li><a href="#" target="_blank">Cato monstravit</a></li>
							</ul>
						</aside>
					</div>
					
				</div>
				
			<?php endwhile; ?>
			<div class="other-personalities">
				<?php 
					$queried_object = get_queried_object();
					$termName = $queried_object->slug;
					
					$argsTaxAuthors = array('taxonomy' => 'tax-personality-index', 'field' => 'slug', 'terms' => $termName, 'operator' => 'IN' );
					
					$argsName = array('post_type' => 'cpt-trombinoscope', 'tax_query' => array( $argsTaxAuthors ), 'orderby' => 'ASC', 'order' => 'DESC' );
					$queryName = new WP_Query( $argsName );
					
					if($queryName->have_posts()): 
				?>
					<ul>
						<?php while($queryName->have_posts()): $queryName->the_post(); ?>
						<li><a href="#" class="<?php echo $post->post_name; ?>"><?php echo get_the_title(); ?></a></li>
						<?php endwhile; ?>
					</ul>
					<?php endif; ?>
				<?php wp_reset_postdata(); ?>
			</div>
		</div>
	</section>
<?php endif; ?>

<?php get_footer(); ?>