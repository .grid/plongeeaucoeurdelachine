<?php
	global $post;
	$parent_slug = get_post($post->post_parent);
	$parent_page_slug = $parent_slug->post_name;
?>
<aside class="featured <?php echo $parent_page_slug; ?>">
	<div class="container">
		<?php if( get_field('acf_chapter_featured_title') ){ ?>
			<div class="extra-text">
				<span class="bg-left"></span>
				<p><?php echo get_field('acf_chapter_featured_title'); ?></p>
				<?php if( get_field('acf_chapter_featured_title_author') ){ ?>
				<span class="author"><?php echo get_field('acf_chapter_featured_title_author'); ?></span>
				<?php }; ?>
				<span class="bg-right"></span>
			</div>
		<?php }else{ ?>
		<div class="extra-text">
			<span class="bg-left"></span>
			<p class="citation">Instants de vie <br />en Chine</p>
			<span class="bg-right"></span>
			<span href="#" class="extra-text-arrow"></span>
		</div>
		
		<?php }; ?>
		<div class="featured-content authors">
		
		<?php
			if( !is_single( '7' ) && get_field('acf_chapter_portraits') ){ 
			$outputPortraits = '';
		?>
		
			<div class="portraits-list">
					<div>
			<?php
				while( has_sub_field('acf_chapter_portraits') ){
					$postID = $post->ID;
					
					$post_object = get_sub_field('acf_chapter_portrait', $postID);
					if( $post_object ): // override $post
						$post = $post_object;
						setup_postdata( $post );
						
						if( get_field('acf_personality_portrait_videotitle') ){
							$videoTitlePortrait = get_field('acf_personality_portrait_videotitle');
						} else {
							$videoTitlePortrait = '';
						};
						
						if( get_field('acf_personality_portrait_videodesc') ){
							$videoDescPortrait = get_field('acf_personality_portrait_videodesc');
						} else {
							$videoDescPortrait = '';
						};
						
						if( get_field('acf_personality_portrait_title') ){
							$videoLabelPortrait = get_field('acf_personality_portrait_title');
						} else {
							$videoLabelPortrait = get_the_title();
						};
						
						$postID = $post->ID;
						
						
						if( get_field('acf_personality_portrait_featured_personalities', $postID ) ){
							$personnalitiesList = '';
							$personnalitiesList .= '<div class="additional-links">';
							
							while( has_sub_field('acf_personality_portrait_featured_personalities', $postID) ){
								
								$post_object = get_sub_field('acf_personality_portrait_featured_personality', $postID);
								if( $post_object ): 
									
									// override $post
									$post = $post_object;
									setup_postdata( $post ); 
									$label = '';
									
										$label .= "Intervenant : ";
									
									
									$label = htmlspecialchars_decode(htmlentities( $label, ENT_NOQUOTES, 'UTF-8'));
									
									$authorTitle = get_the_title();
									$authorCleanTitle = htmlentities( $authorTitle, ENT_NOQUOTES, 'UTF-8');
									
									$personnalitiesList .= '<p><span class="title">' . $label . '</span>' . $authorCleanTitle . '</p><p><a href="' . home_url() . '/biographies/#' . $post->post_name . '" class="popin">Biographie</a>';
									
									
									if( get_field('acf_personnalites_bibliographie') && get_field('acf_personnalites_bibliographie') != '' ){
										
										$personnalitiesList .= ' - <a href="' . home_url() . '/bibliographie/#' . $post->post_name . '" class="popin">Bibliographie</a></p>';
									
									};
									
									
								    //wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly
								endif;
								
								
							};
							$personnalitiesList .= '</div>';
							//var_dump($personnalitiesList);
							wp_reset_postdata();
						} else { $personnalitiesList = ''; };
						
						
						$dataVideoChapters = '';
						if( get_field('acf_portrait_video_id', $postID) && get_field('acf_portrait_video_id', $postID) != ' ' ){
							
							$videoID = get_field('acf_portrait_video_id', $postID);
							
							$dataVideoChapters .= "<script type='text/javascript'>
							ChapterMarkerPlayer.insert({
								container: 'player',
								videoId: '" . $videoID . "',
								width: '560',
								height: '315',";
							
							if( get_field('acf_portrait_video_marker_timecodes', $postID) ){
								
								$dataVideoChapters .= "chapters: {";
								$dataVideoChaptersList = '';
								
								while( has_sub_field('acf_portrait_video_marker_timecodes', $postID) ){
									$markerTitle = get_sub_field('acf_portrait_video_marker_title', $postID);
									$markerTimecode = get_sub_field('acf_portrait_video_marker_timecode', $postID);
									
									
									
									$dataVideoChaptersList .= "'" . $markerTimecode . "': '" . $markerTitle . "', ";
									
								};
								
								
								
								$dataVideoChapters .= $dataVideoChaptersList . "}";
							};
							
							$dataVideoChapters .= "}); </script>";
							
							
							
						} else { $dataVideoChapters = ''; };
												
						if( get_field('acf_personality_portrait_videolink', $postID) ){
							$videoLinkPortrait = get_field('acf_personality_portrait_videolink', $postID);
							$outputPortraits .= '<a href="#" data-video="' . htmlentities( $videoLinkPortrait ) . '" data-video-title="' . $videoTitlePortrait . '" data-video-description="' . $videoDescPortrait . '" data-video-authors="' . htmlentities( $personnalitiesList ) . '" data-video-chapters="' . html_entity_decode($dataVideoChapters) . '">';
						};
						
						
						$outputPortraits .= "<span class='featured-personnality'>";
						$outputPortraits .= "<span>" . $videoLabelPortrait . "</span>";
						
						if( get_field('acf_personality_portrait_thumb', $postID) ){
							$personalityThumbID = get_field('acf_personality_portrait_thumb', $postID);
							$sizeThumb = 'thumb-story';
							$personalityThumb = wp_get_attachment_image_src( $personalityThumbID, $sizeThumb );
							
							$outputPortraits .= "<img src=" . $personalityThumb[0] . " />";
						};
						
						
						$outputPortraits .= "</span>";
						
						
						if( get_field('acf_personality_portrait_videolink', $postID) ){
							$outputPortraits .= "</a>";
						};
				
					wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly 
					endif;
				};
				
				echo $outputPortraits;
			?>
					</div>
			</div>
		<?php
		} elseif( is_single( '7' ) && get_field('acf_chapter_stories') ){
		
			$outputStories = '';
		?>
		
			<div class="portraits-list">
					<div>
			<?php
				while( has_sub_field('acf_chapter_stories') ){
					
					$post_object = get_sub_field('acf_chapter_story');
					if( $post_object ): // override $post
						$post = $post_object;
						setup_postdata( $post );
						
						
						
						$postID = $post->ID;
						
						
						
						
						if( get_field('acf_personality_story_videotitle', $postID) ){
							$videoTitleStory = get_field('acf_personality_story_videotitle');
						} else {
							$videoTitleStory = '';
						};
						
						if( get_field('acf_personality_story_videodesc', $postID) ){
							$videoDescStory = get_field('acf_personality_story_videodesc');
						} else {
							$videoDescStory = '';
						};
						
						if( get_field('acf_personality_story_title', $postID) ){
							$videoLabelStory = get_field('acf_personality_story_title');
						} else {
							$videoLabelStory = get_the_title();
						};
						
						if( get_field('acf_video_featured_personalities', $postID ) ){
							$personnalitiesList = '';
							$personnalitiesList .= '<div class="additional-links">';
							
							while( has_sub_field('acf_video_featured_personalities', $postID) ){
								
								$post_object = get_sub_field('acf_video_featured_personality', $postID);
								if( $post_object ): 
									
									// override $post
									$post = $post_object;
									setup_postdata( $post ); 
									$label = '';
									
									$label .= "Intervenant : ";
									
									
									$label = htmlspecialchars_decode(htmlentities( $label, ENT_NOQUOTES, 'UTF-8'));
									
									$authorTitle = get_the_title();
									$authorCleanTitle = htmlentities( $authorTitle, ENT_NOQUOTES, 'UTF-8');
									
									$personnalitiesList .= '<p><span class="title">' . $label . '</span>' . $authorCleanTitle . '</p><p><a href="' . home_url() . '/biographies/#' . $post->post_name . '" class="popin">Biographie</a>';
									
									if( get_field('acf_personnalites_bibliographie') && get_field('acf_personnalites_bibliographie') != '' ){
										
										$personnalitiesList .= ' - <a href="' . home_url() . '/bibliographie/#' . $post->post_name . '" class="popin">Bibliographie</a></p>';
									
									};
								    
								    //wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly
								endif;
								
								
							};
							$personnalitiesList .= '</div>';
							
							wp_reset_postdata();
						} else { $personnalitiesList = ''; };
						
						
				
			
						
						if( get_field('acf_personality_story_videolink', $postID) ){
							$videoLinkStory = get_field('acf_personality_story_videolink', $postID);
							$outputStories .= "<a href='#' data-video=' $videoLinkStory ' class='story' data-video-title='" . $videoTitleStory . "' data-video-description='" . $videoDescStory . "' data-video-authors='" . $personnalitiesList . "'>";
						};
						
						$outputStories .= "<span class='featured-personnality'>";
						$outputStories .= "<span>" . $videoLabelStory . "</span>";
						
						if( get_field('acf_personality_story_thumb', $postID) ){
							$personalityThumbID = get_field('acf_personality_story_thumb', $postID);
							$sizeThumb = 'thumb-story';
							$personalityThumb = wp_get_attachment_image_src( $personalityThumbID, $sizeThumb );
							
							$outputStories .= "<img src=" . $personalityThumb[0] . " />";
						};
						
						
						$outputStories .= "</span>";
						
						
						if( get_field('acf_personality_story_videolink', $postID) ){
							$outputStories .= "</a>";
						};
				
					wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly 
					endif;
				};
				
				echo $outputStories;
			?>
					</div>
			</div>
		<?php
		
		};
		
		?>
		
			<a href="#" class="btn-next-personnalities"><span>Suivant</span></a>
		</div>
	</div>
</aside>