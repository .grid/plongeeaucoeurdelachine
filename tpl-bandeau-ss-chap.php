<aside class="featured">
	<div class="container">
		
		<div class="extra-text">
			<?php if( get_field('acf_chapter_featured_title') ){ ?>
				<span class="bg-left"></span>
				<p><?php echo get_field('acf_chapter_featured_title'); ?></p>
				<?php if( get_field('acf_chapter_featured_title_author') ){ ?>
				<span class="author"><?php echo get_field('acf_chapter_featured_title_author'); ?></span>
				<?php }; ?>
				<span class="bg-right"></span>
				<!--<span href="#" class="extra-text-arrow"></span>-->
			<?php }; ?>
		</div>
		<?php if( get_field('acf_chapter_featured_text') ){ ?>
		<div class="featured-content">
			<p><?php echo get_field('acf_chapter_featured_text'); ?></p>
		</div>
		<?php }; ?>
		
	</div>
</aside>