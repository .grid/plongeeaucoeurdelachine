<?php
			
			$label = 'Intervenant : ';
			
			$videoLink1 = '<iframe width="560" height="315" src="http://www.youtube.com/embed/fnbXFYwlBt0?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>';
			$videoTitle1 = 'Arts culinaires';
			$videoAuthor1 = 'Bernard Fernandez';
			$videoDescription1 = '<p>La France et la Chine partagent une même très ancienne tradition de l’art culinaire. En Chine, nous dit Bernard Fernandez, lorsque quelque chose de fondamental se passe, le rituel c’est le repas, c’est le moment où l’on va dire des choses sans les dire...<p>';
			$videoAuthors1 = '<div class="additional-links"><p><span class="title">' . $label . '</span>' . $videoAuthor1 . '</p><p><a href="' . home_url() . '/biographies/#bernard-fernandez" class="popin">Biographie</a> - <a href="' . home_url() . '/bibliographie/#bernard-fernandez" class="popin">Bibliographie</a></p></div>';
			
			
			$videoItem1 = '<li><a href="#" data-video="' . htmlentities( $videoLink1 ) . '" data-video-title="' . $videoTitle1 . '" data-video-description="' . $videoDescription1 . '" data-video-authors="' . htmlentities( $videoAuthors1 ) . '">' . $videoTitle1 . '<span>' . $videoAuthor1 . '</span></a></li>';
			
			
			$videoLink2 = '<iframe width="560" height="315" src="http://www.youtube.com/embed/7fVX2698Mao?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>';
			$videoTitle2 = 'Eldorado';
			$videoAuthor2 = 'Thomas Chabrières';
			$videoDescription2 = '<p>20 000 français à Shanghaï aujourd’hui, cette ville serait-elle devenue un nouvel Eldorado ? qui sont ces « arrivants », qu’attendent-ils de cette aventure ? Thomas Chabrières qui a lui même tenté sa chance, nous en dit plus ...<p>';
			$videoAuthors2 = '<div class="additional-links"><p><span class="title">' . $label . '</span>' . $videoAuthor2 . '</p><p><a href="' . home_url() . '/biographies/#thomas-chabrieres" class="popin">Biographie</a> - <a href="' . home_url() . '/bibliographie/#thomas-chabrieres" class="popin">Bibliographie</a></p></div>';
			$videoChapters2 = "<script type='text/javascript'>
					ChapterMarkerPlayer.insert({
						container: 'player',
						videoId: '7fVX2698Mao',
						width: '560',
						height: '315',
						chapters: {
							'0' : 'Arrivée à Shanghaï',
							'50' : 'Profil type de l\'expatrié',
							'95' : 'La Chine est contagieuse',
							'148' : 'Les attentes des Français',
							'196' : 'Un partenaire chinois'
						} }); </script>";
			
			$videoItem2 = '<li><a href="#" data-video="' . htmlentities( $videoLink2 ) . '" data-video-title="' . $videoTitle2 . '" data-video-description="' . $videoDescription2 . '" data-video-authors="' . htmlentities( $videoAuthors2, ENT_QUOTES, 'UTF-8') . '" data-video-chapters="' . $videoChapters2 . '">' . $videoTitle2 . '<span>' . $videoAuthor2 . '</span></a></li>';
			
			$videoLink3 = '<iframe width="560" height="315" src="http://www.youtube.com/embed/XTx48N6SI64?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>';
			$videoTitle3 = 'Croisements';
			$videoAuthor3 = 'Suyan Zhou';
			$videoDescription3 = '<p>Les chinois n’ont jamais hésité à aller chercher les lumières à l’étranger ni à partager leurs propres inventions avec le reste du monde, nous fait remarquer, entre autres choses,  Suyan Zhou ...<p>';
			$videoAuthors3 = '<div class="additional-links"><p><span class="title">' . $label . '</span>' . $videoAuthor3 . '</p><p><a href="' . home_url() . '/biographies/#suyan-zhou" class="popin">Biographie</a> - <a href="' . home_url() . '/bibliographie/#suyan-zhou" class="popin">Bibliographie</a></p></div>';
			$videoLexique3 = '<p class="additional-definitions"><span class="title">Lexique : </span><a class="popin" href="' . home_url() . '/lexique/#confucianisme">Confucianisme</a> <a class="popin" href="' . home_url() . '/lexique/#taoisme">Taoisme</a> <a class="popin" href="' . home_url() . '/lexique/#bouddhisme">Bouddhisme</a> <a class="popin" href="' . home_url() . '/lexique/#harmonie">Harmonie</a> </p>';
			$videoChapters3 = "<script type='text/javascript'>
					ChapterMarkerPlayer.insert({
						container: 'player',
						videoId: 'XTx48N6SI64',
						width: '560',
						height: '315',
						chapters: {
							'0' : 'Chercher la lumière à l\'étranger',
							'103' : 'Les Chinois, des copieurs ?',
							'171' : 'Pragmatisme chinois',
							'291' : 'Place des religions'
						} }); </script>";
			
			$videoItem3 = '<li><a href="#" data-video="' . htmlentities( $videoLink3 ) . '" data-video-title="' . $videoTitle3 . '" data-video-description="' . $videoDescription3 . '" data-video-authors="' . htmlentities( $videoAuthors3 ) . '" data-video-lexique="' . htmlentities( $videoLexique3 ) . '" data-video-chapters="' . $videoChapters3 . '">' . $videoTitle3 . '<span>' . $videoAuthor3 . '</span></a></li>';
			
			if( is_single('regards-croises')){
				$class = 'current-menu-item';
				$regardsCroises = '<li class="' . $class . '">';
			} else{
				$regardsCroises = '<li>';
			};
			
			$chapterRegargsCroises = get_permalink(7);
			
			$regardsCroises .= '<a href="' . $chapterRegargsCroises . '">Regards <span>croisés</span></a> <ul class="sub-menu regards-croises">' . $videoItem1 . $videoItem2 . $videoItem3 . '</ul>';
			$regardsCroises .= '</li>';

	$defaults = array(
		//'theme_location'  => 'Header',
		'menu'            => '2',
		'container'       => 'nav',
		'container_class' => '',
		'container_id'    => 'main-menu',
		'menu_class'      => 'menu',
		'menu_id'         => '2',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul id="%1$s" class="top-menu %2$s">%3$s' . $regardsCroises . '</ul>',
		'depth'           => 0,
		//'walker'          => new description_walker
	);
	wp_nav_menu( $defaults );
?>