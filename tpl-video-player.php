<div class="video-wrapper">
	<div class="hidden-player">
		<section id="video-player">
			<span class="btn-close">fermer</span>
			
			<div id="player"></div>
			
			<?php 
				
				global $post;
				
				$pageURL = get_permalink( $post );
				
			?>
			
			<div class="share video">
				<iframe src="//www.facebook.com/plugins/like.php?href=<?php echo htmlentities( $pageURL ); ?>&amp;send=false&amp;layout=button_count&amp;width=100&amp;show_faces=false&amp;font&amp;colorscheme=dark&amp;action=like&amp;height=21&amp;appId=638699199490003" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100px; height:21px;" allowTransparency="true"></iframe>
				<a href="https://twitter.com/share" class="twitter-share-button" data-url="<?php echo htmlentities( $pageURL ); ?>" data-text="Plongée au cœur de la Chine" data-hashtags="webdocu">Tweet</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
			</div>
			<span class="youtube-hd"></span>
			<span class="youtube-hd-message">Ce contenu est disponible au format HD</span>
			<aside>
			
			</aside>
		</section>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		function onYouTubeIframeAPIReady() {
		  var player;
		  player = new YT.Player('player', {
		    videoId: 'M7lc1UVf-VE',
		    playerVars: { 'autoplay': 1, 'controls': 0 },
		    events: {
		      'onReady': onPlayerReady,
		      'onPlaybackQualityChange': onPlayerPlaybackQualityChange,
		      'onStateChange': onPlayerStateChange,
		      'onError': onPlayerError
		    }
		  });
		}
	
	
	

	$('.youtube-hd').on('click', function(){
		
	});
		
		
	});
</script>